# Table Hero - coding challenge
### Code organisation: 
The code is organised like the following structure 

*  View Controllers
   -    Master View Controllers
   -     Detail View Controllers
*  Views
*  Models
   -  Managers
*  DataStructure
*  Categories
*  Assets

### Functional Spec
##### Original Spec
* A 'drag and drop' interface is required. The User must be able to pick from at least 3 different furniture elements and place the elements on a part of the screen representing the floor.
* Any number of furniture elements can be placed on a floor in a single layout as long as the elements do not overlap.
* Each furniture element in a layout should have a text field to give it a name.
* Each layout should have a text field to give it a name.
* 'New' button to create a fresh (empty) layout. Pressing 'New' should discard the layout currently on the screen (unless it has been explicitly saved)
* 'Save' button to save the layout that is currently on the screen.
* 'Browse' button to present the user with a list of  saved layouts. Clicking on the name of a previously saved layout should display it in the same area used for creating layouts.
* Furniture elements placed on a layout can be moved around further, until the 'Save' button is pressed. Once a layout is saved, it cannot be edited.
* The saved layouts must persist between app re-starts.
* Only the landscape orientation needs to be supported.

##### Changes
* Application by default opens in browse mode
* Application will open up Layout editor on tapping on the add button
* Saving or cancelling in an edited layout will save the layout and navigate back to the browser screen.

### Design Considerations
* The application only for ipad  and only for landscape mode
* The layout will be stored locally and will not sync  multiple 
* Because we considered the layouts to be  once it is created and consider that it will be local to a  device. We have decided to store the Layout structure as image and store the related object in User default store.  
* We used a quadtree (of depth 2) to minimise search for collision  for on layout floor.

### Dependencies

 target 8.0

### Todos

- Write Unit Tests
- Code Commenting

License
-------
##### "THE BEER-WARE LICENSE":
<me@raysourav.com> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. -  Ray
---------------------------------------------------------------------------------------------------------