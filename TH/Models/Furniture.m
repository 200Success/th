//
//  Furniture.m
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "Furniture.h"

@implementation Furniture

+ (instancetype) instanceWithProperties:(NSDictionary*) properties {
    return [[self alloc]initWithProperties:(NSDictionary*) properties];
}

- (instancetype) initWithProperties:(NSDictionary*) properties {
    self = [super init];
    if (self) {
        _name = [properties objectForKey:@"Name"];
        _img = [properties objectForKey:@"Image"];
    }
    return self;
}

@end
