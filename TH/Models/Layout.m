//
//  Layout.m
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "Layout.h"

@implementation Layout

- (instancetype) init {
    self = [super init];
    if(self) {
        _funitures = [NSMutableArray array];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        _name = [decoder decodeObjectForKey:@"name"];
        _uuid = [decoder decodeObjectForKey:@"uuid"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeObject:_uuid forKey:@"uuid"];
}

@end
