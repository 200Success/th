//
//  Layout.h
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Layout : NSObject <NSCoding>

@property (copy, nonatomic) NSString * name;
@property (copy, nonatomic) NSString * uuid;
@property (copy, nonatomic) NSMutableArray *funitures;

@end
