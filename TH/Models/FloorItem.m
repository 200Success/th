//
//  FloorItem.m
//  TH
//
//  Created by Samyak Bhuta on 21/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "FloorItem.h"
#import "CATextLayer+TH.h"

@implementation FloorItem

+ (instancetype) instanceWithProperties:(NSDictionary*) properties {
    return [[self alloc]initWithProperties:(NSDictionary*) properties];
}

- (instancetype) initWithProperties:(NSDictionary*) properties {
    self = [super init];
    if (self) {
        _name = [properties objectForKey:@"Name"];
        _img = [properties objectForKey:@"Image"];
        _imageLayer = nil;
        _textLayer = nil;
        _heighlighLayer = nil;
        _rotation = 0.0;
        _b = nil;
    }
    return self;
}

#pragma  mark - Bounding Box protocol methods
- (void) prepareBoundingBox {
    if(_imageLayer) {
        _b = [BoundingBox instanceWithRect:_imageLayer.frame];
    }
    
}

- (BoundingBox *)getBoundingBox {
    return _b;
}

#pragma  mark - UI methods
- (void) prepareImageLayerWithFrame:(CGRect) rect {
    if(_img) {
        _imageLayer = [CALayer layer];
        _imageLayer.backgroundColor = [[UIColor clearColor] CGColor];
        _imageLayer.contents = (id)[[UIImage imageNamed:_img] CGImage];
        [_imageLayer setFrame:rect];
    }
}

- (void) setTextLayer {
    if(_textLayer) {
        [self removeTextLayer];
    }
    if(_name) {
        _textLayer = [CATextLayer layer];
        _textLayer.wrapped = YES;
        _textLayer.string = _name;
        _textLayer.alignmentMode =kCAAlignmentCenter;
        _textLayer.bounds = _imageLayer.bounds;
        _textLayer.fontSize = 18.0f;
        [_textLayer adjustToFit];
        _textLayer.position = CGPointMake(_imageLayer.bounds.size.width/2.0f,
                                         _imageLayer.bounds.size.height/2.0f) ;
        [_imageLayer addSublayer:_textLayer];
    }
}

- (void) removeTextLayer {
    [_textLayer removeFromSuperlayer];
    _textLayer =nil;
    
}

- (void) highlightWithColor:(UIColor *) color {
    if(_heighlighLayer) {
        [self dehighlight];
    }
    _heighlighLayer = [CALayer layer];
    [_heighlighLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    _heighlighLayer.bounds = _imageLayer.bounds;
    _heighlighLayer.position = CGPointMake(_imageLayer.bounds.size.width/2.0f,
                                      _imageLayer.bounds.size.height/2.0f) ;
    [_heighlighLayer setBorderWidth:1.0];
    [_heighlighLayer setBorderColor:[color CGColor]];
    [_imageLayer addSublayer:_heighlighLayer];
}

- (void) dehighlight {
    [_heighlighLayer removeFromSuperlayer];
    _heighlighLayer =nil;
}

- (void) rotateLeft {
    _rotation -= 90.0;
    _imageLayer.transform = CATransform3DMakeRotation(_rotation / 180.0 * M_PI, 0.0, 0.0, 1.0);
}

- (void) rotateRight {
    _rotation += 90.0;
    _imageLayer.transform = CATransform3DMakeRotation(_rotation / 180.0 * M_PI, 0.0, 0.0, 1.0);
}

- (void) remove {
    [_imageLayer removeFromSuperlayer];
}
@end
