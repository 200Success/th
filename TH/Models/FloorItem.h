//
//  FloorItem.h
//  TH
//
//  Created by Samyak Bhuta on 21/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Quadtree.h"

@interface FloorItem : NSObject<BoundingBoxProtocol> {
    BoundingBox * _b;
    CATextLayer * _textLayer;
    CALayer * _heighlighLayer;
    CGFloat _rotation;
}

@property (copy, nonatomic) NSString * name;
@property (copy, nonatomic) NSString * img;
@property (assign, nonatomic) CALayer *imageLayer;

+ (instancetype) instanceWithProperties:(NSDictionary*) properties;
- (instancetype) initWithProperties:(NSDictionary*) properties;

#pragma  mark - UI methods
- (void) prepareImageLayerWithFrame:(CGRect) rect;
- (void) setTextLayer;
- (void) removeTextLayer;
- (void) highlightWithColor:(UIColor *) ccolor;
- (void) dehighlight;
- (void) rotateLeft;
- (void) rotateRight;
- (void) remove ;
@end
