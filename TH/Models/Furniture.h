//
//  Furniture.h
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Furniture : NSObject 

@property (copy, nonatomic) NSString * name;
@property (copy, nonatomic) NSString * img;
@property (assign, nonatomic) CALayer *layer;

+ (instancetype) instanceWithProperties:(NSDictionary*) properties; 
- (instancetype) initWithProperties:(NSDictionary*) properties;
@end
