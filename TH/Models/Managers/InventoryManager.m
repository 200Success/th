//
//  InventoryManager.m
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "InventoryManager.h"

@implementation InventoryManager

+ (instancetype)sharedManager {
    static InventoryManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[InventoryManager alloc] init];
    });
    return sharedMyManager;
}


- (instancetype) init {
    self=[super init];
    if(self) {
        @autoreleasepool {
            NSArray *itemsList = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Inventory" ofType:@"plist"]];
            _inventory = [NSMutableArray array];
            for(NSDictionary *item in itemsList) {
                Furniture * furniture = [Furniture instanceWithProperties:item];
                if(furniture) {
                    [_inventory addObject:furniture];
                }
            }
        }
    }
    return self;
}
@end
