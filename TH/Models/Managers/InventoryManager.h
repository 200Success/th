//
//  InventoryManager.h
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Furniture.h"
@interface InventoryManager : NSObject

@property (strong, nonatomic) NSMutableArray *inventory;

+ (instancetype)sharedManager;

@end
