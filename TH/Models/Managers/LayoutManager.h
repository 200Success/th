//
//  LayoutManager.h
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Layout.h"
#import "FloorItem.h"
#import <UIKit/UIKit.h>

@interface LayoutManager : NSObject {
    NSMutableArray *_layouts;
}

@property (strong, readonly) NSMutableArray *layouts;

+ (instancetype) sharedManager;
+ (NSString *)uuid;
+ (BOOL) saveImage:(UIImage *)image
       withImageId:(NSString *)imageId;
+ (UIImage *) imageByImageId:(NSString *) imageId;
- (void) addLayout:(Layout *)layout;
- (void) save;

@end
