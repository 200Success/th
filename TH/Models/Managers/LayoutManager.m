//
//  LayoutManager.m
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "LayoutManager.h"

@implementation LayoutManager


+ (instancetype) sharedManager {
    static LayoutManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[LayoutManager alloc] init];
    });
    return sharedMyManager;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"layouts"];
        _layouts = [[NSKeyedUnarchiver unarchiveObjectWithData:data] mutableCopy];
        if(_layouts==nil) {
            _layouts = [NSMutableArray array];
        }
    }
    return self;
}

- (void) addLayout:(Layout *)layout {
    [_layouts addObject:layout];
}

- (void) save {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_layouts];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"layouts"];
}


+ (BOOL) saveImage:(UIImage *)image
       withImageId:(NSString *)imageId {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:
                                        [NSString stringWithFormat: @"%@.png", imageId]];
    return [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
}

+ (UIImage *) imageByImageId:(NSString *) imageId {
    UIImage *image = nil;
    @autoreleasepool {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:
                              [                 NSString stringWithFormat: @"%@.png", imageId]];
        image = [UIImage imageWithContentsOfFile:filePath];
    }
    return image;
}

+ (NSString *)uuid {
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}


@end
