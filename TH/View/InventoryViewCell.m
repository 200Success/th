//
//  InventoryViewCell.m
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "InventoryViewCell.h"

@implementation InventoryViewCell


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        [self setBackgroundColor:[UIColor colorWithRed:230.0f/255.0f
                                                 green:230.0f/255.0f blue:230.0f/255.0f
                                                 alpha:1.0f]
         ];
        
        _furnitureLable = [[UILabel alloc] initWithFrame:CGRectZero];
        _furnitureImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_furnitureImg];
        [self.contentView addSubview:_furnitureLable];
    }
    return self;
}


- (void) setImage:(UIImage *) image {
    CGSize cellSize = self.frame.size;
    CGSize imgSize = image.size;
    [_furnitureImg setFrame:CGRectMake((cellSize.width-imgSize.width)/2.0,
                                       (cellSize.height-imgSize.height)/2.0,
                                       imgSize.width, imgSize.height)
     ];
    [_furnitureImg setImage:image];
    [_furnitureImg setAlpha:0.75f];
    
}

- (void) setTitle:(NSString *)title {
    UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    CGSize cellSize = self.frame.size;
    
    CGSize labelSize = [title sizeWithFont:titleFont
                                constrainedToSize:self.contentView.frame.size
                                    lineBreakMode:NSLineBreakByWordWrapping];
    
    [_furnitureLable setFrame:CGRectMake((cellSize.width-labelSize.width)/2.0,
                                       (cellSize.height-labelSize.height)/2.0,
                                       labelSize.width, labelSize.height)
     ];
    [_furnitureLable setFont:titleFont];
    [_furnitureLable setText:title];
    [_furnitureLable setBackgroundColor: [UIColor colorWithRed:1.0
                                                      green:1.
                                                       blue:1.0
                                                      alpha:0.9]];
}

- (void) highlightSelection {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [_furnitureImg setAlpha:0.3f];
    [_furnitureLable setAlpha:0.0f];
    [UIView commitAnimations];
}

- (void) dehighlightSelection {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4];
    [_furnitureImg setAlpha:0.8f];
    [_furnitureLable setAlpha:1.0f];
    [UIView commitAnimations];
}

@end
