//
//  InventoryViewCell.h
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InventoryViewCell : UITableViewCell {
    UIImageView *_furnitureImg;
}

@property (retain, nonatomic) UIImageView *furnitureImg;
@property (retain, nonatomic) UILabel *furnitureLable;

- (void) setImage:(UIImage *) image;
- (void) setTitle:(NSString *)title;
- (void) highlightSelection;
- (void) dehighlightSelection;
@end
