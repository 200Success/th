//
//  BrowserViewController.m
//  TH
//
//  Created by Samyak Bhuta on 18/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "BrowserViewController.h"
#import "LayoutManager.h"
@interface BrowserViewController ()

@end

@implementation BrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(_layout) {
        [_layoutImage setImage:[LayoutManager imageByImageId:_layout.uuid]];
        [_layoutTitle setText:_layout.name];
    }
}

@end
