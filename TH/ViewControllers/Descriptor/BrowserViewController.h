//
//  BrowserViewController.h
//  TH
//
//  Created by Samyak Bhuta on 18/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Layout;
@interface BrowserViewController : UIViewController

@property (assign, readwrite) IBOutlet UILabel *layoutTitle;
@property (assign, readwrite) IBOutlet UIImageView *layoutImage;
@property (assign, nonatomic)  Layout *layout;
@end
