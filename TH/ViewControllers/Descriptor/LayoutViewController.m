//
//  LayoutViewController.m
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "LayoutViewController.h"
#import "Quadtree.h"
#import "LayoutManager.h"
#import "Furniture.h"
#import "CATextLayer+TH.h"

@interface LayoutViewController ()

@end

@implementation LayoutViewController

#pragma mark - VC methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerHandlerForTouchGesture];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _layout = [[Layout alloc] init];
    _isEditingText=NO;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     _q = [Quadtree rootNodeWithRect:_layoutView.bounds];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    _q=nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - keyboard event observer methods

- (void)keyboardWillShow:(NSNotification *)note {
    if(_isEditingText) {
        id _obj = [note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect _keyboardFrame = CGRectNull;
        if ([_obj respondsToSelector:@selector(getValue:)]) [_obj getValue:&_keyboardFrame];
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_inputView  setFrame: CGRectMake(_layoutTools.frame.origin.x,
                                              _layoutTools.frame.origin.y - _keyboardFrame.size.height,
                                              _layoutTools.frame.size.width,
                                              _layoutTools.frame.size.height)];
        } completion:nil];
    }
}

- (void)keyboardWillHide:(NSNotification *)note {
    if(_isEditingText) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_inputView  setFrame: _layoutTools.frame];
        } completion:nil];
    }
}



#pragma mark - gseture recognizer methods

- (void) registerHandlerForTouchGesture {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(tap:)];
    
    [self.layoutView addGestureRecognizer:tap];
    [tap setDelegate:self];
}

- (IBAction)tap:(UIGestureRecognizer *)sender {
    CGPoint point =[sender locationInView:self.layoutView];
    if(![self isLayoutItemSelected]) {
        if([self selectItemAtPoint:point]) {
            [_layoutTools setHidden:NO];
        }
    } else if(CGRectContainsPoint(_selectedItem.imageLayer.frame, point)) {
        [self deselectItemt];
        [self clearEditView];
        [_layoutTools setHidden:YES];
    }
}

#pragma mark - method to select  layout items
- (BOOL) selectItemAtPoint:(CGPoint) point {
    _selectedItem = (FloorItem *)[_q objectAtPoint:point];
    if(_selectedItem) {
        [_selectedItem highlightWithColor:[UIColor blueColor]];
        [_q remove:_selectedItem];
        return YES;
    }
    return NO;
}

- (void) deselectItemt {
    if(_selectedItem) {
        [_selectedItem dehighlight];
        [_q insert:_selectedItem];
        _selectedItem = nil;
    }
}

- (void) removeItem {
    if(_selectedItem) {
        [_selectedItem remove];
        _selectedItem = nil;
    }
}

#pragma  mark -  text edit bar methods
- (void) prepareTextEditBar {
    if(_inputView==nil) {
        _inputView = [[UIView alloc] initWithFrame:_layoutTools.frame];
        [_inputView setBackgroundColor:[UIColor colorWithRed:230.0f/255.0f
                                                       green:230.0f/255.0f
                                                        blue:230.0f/255.0f
                                                       alpha:1.0]];
        
        _inputField = [[UITextField alloc] initWithFrame: CGRectMake(20, 8, _layoutTools.bounds.size.width-200, _layoutTools.bounds.size.height-16) ];
        [_inputField setBackgroundColor:[UIColor whiteColor]];
        [_inputView addSubview:_inputField];
        
        
        UIButton *done=[UIButton buttonWithType:UIButtonTypeCustom];
        done.frame=CGRectMake(_layoutTools.bounds.size.width-160 ,8, 140, _layoutTools.bounds.size.height-16);
        [done setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [done setTitle:@"Done" forState:UIControlStateNormal];
        [done addTarget:self  action:@selector(editItemText) forControlEvents:UIControlEventTouchUpInside];
        [_inputView addSubview:done];
        
    }
}

-(void) clearEditView {
    _isEditingText = NO;
    [_inputView removeFromSuperview];
}


#pragma  mark - layout protocol methods

- (void) addFurnitureToLayoutl:(Furniture *)furniture withFrame:(CGRect)frame {
    
    if(furniture.name!=nil &&
       furniture.img!=nil) {
        FloorItem * item = [FloorItem instanceWithProperties:@{
                                                               @"Name":furniture.name,
                                                               @"Image":furniture.img
                                                               }];
        [item prepareImageLayerWithFrame:frame];
        [item setTextLayer];
        [[_layoutView layer] addSublayer:item.imageLayer];
        [_q insert:item];
    }
}

- (BOOL) isLayoutItemSelected {
    if(_selectedItem==nil) {
        return NO;
    }
    return YES;
}

#pragma  mark - methods for manipulating layer of the layout view

- (BOOL) checkForOverlappingRect:(CGRect) frame {
    return[_q doesIntersect:frame];
}

- (UIImage *)imageFromLayer:(CALayer *)layer {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions([layer frame].size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext([layer frame].size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}

#pragma mark - user actions handler

- (IBAction)saveAction:(id)sender {
    if(![self isLayoutItemSelected]) {
        if(_layoutTitle.text.length) {
            [_layout setName:_layoutTitle.text];
            UIImage *image = [self imageFromLayer:[_layoutView layer]];
            NSString *uuid = [LayoutManager uuid];
            if([LayoutManager saveImage:image withImageId:uuid]) {
                [_layout setUuid:uuid];
                [[LayoutManager sharedManager]  addLayout:_layout];
                [[LayoutManager sharedManager] save];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseLayoutEditor" object:nil];
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please enter a name for your layout."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (IBAction)cancelAction:(id)sende {
    if(![self isLayoutItemSelected]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseLayoutEditor" object:nil];
    }
}

- (IBAction)deleteAction:(id)sender {
    [self removeItem];
    [_layoutTools setHidden:YES];
}

- (IBAction)editAction:(id)sender {
    [self prepareTextEditBar];
    _isEditingText =YES;
    [_inputView setFrame:_layoutTools.frame];
    [_inputField setText:_selectedItem.name];
    [_selectedItem removeTextLayer];
    [self.view addSubview:_inputView];
}

- (void) editItemText {
    if(_selectedItem) {
        [self clearEditView];
        [_selectedItem setName:_inputField.text];
        [_selectedItem setTextLayer];
    }
}

- (IBAction)rotateToRightAction:(id)sender {
    [_selectedItem rotateRight];
    if([_q doesIntersect: _selectedItem.imageLayer.frame]) {
        [_selectedItem highlightWithColor:[UIColor redColor]];
    } else {
        [_selectedItem highlightWithColor:[UIColor blueColor]];
    }
}

- (IBAction)rotateToLeftAction:(id)sender {
    [_selectedItem rotateLeft];
    if([_q doesIntersect: _selectedItem.imageLayer.frame]) {
        [_selectedItem highlightWithColor:[UIColor redColor]];
    } else {
        [_selectedItem highlightWithColor:[UIColor blueColor]];
    }
}

- (IBAction)doneEdittingAction:(id)sender {
    [self deselectItemt];
    [_layoutTools setHidden:YES];
}




@end
