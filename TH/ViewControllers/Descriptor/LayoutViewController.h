//
//  LayoutViewController.h
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutProtocol.h"
@class Layout, Quadtree, FloorItem;
@interface LayoutViewController : UIViewController <UIGestureRecognizerDelegate, LayoutProtocol>{
    FloorItem * _selectedItem;
    UIView * _inputView;
    UITextField *_inputField;
    BOOL _isEditingText;
}

@property (strong, nonatomic) Layout *layout;
@property (strong, nonatomic) Quadtree *q;
@property (assign,readwrite) IBOutlet UIView *layoutView;
@property (assign, nonatomic) IBOutlet UIToolbar *layoutTools;
@property (assign,readwrite) IBOutlet NSLayoutConstraint *toolbarBottomConstrin;
@property (assign, nonatomic) IBOutlet UITextField *layoutTitle;

- (BOOL) checkForOverlappingRect:(CGRect) frame;
- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)deleteAction:(id)sender;
- (IBAction)editAction:(id)sender;
- (IBAction)rotateToRightAction:(id)sender;
- (IBAction)rotateToLeftAction:(id)sender;
- (IBAction)doneEdittingAction:(id)sender;
@end
