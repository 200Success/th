//
//  LayoutBrowserViewController.m
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "LayoutBrowserViewController.h"
#import "LayoutManager.h"
#import "Layout.h"
#import "BrowserViewController.h"
@interface LayoutBrowserViewController ()

@end

@implementation LayoutBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:@"Browse Layouts"];
    _layouts = [LayoutManager sharedManager].layouts;
    [self.tableView reloadData];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if([_layouts count]>1) {
        NSInteger latestLayoutIndex = [_layouts count] - 1;
        [self.tableView selectRowAtIndexPath: [NSIndexPath indexPathForRow:latestLayoutIndex inSection:0]
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
        Layout *layout = _layouts[latestLayoutIndex];
        [self performSegueWithIdentifier:@"DisplayLayout" sender:layout];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if([self.layouts count] > 0) {
        return [self.layouts count];
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =  nil;
    if([self.layouts count] > indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell" forIndexPath:indexPath];
        Layout *layout = (self.layouts)[indexPath.row];
        cell.textLabel.text = layout.name;
    } else  {
        cell = [tableView dequeueReusableCellWithIdentifier:@"SlateCell" forIndexPath:indexPath];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self.layouts count] == 0) {
        return 160;
    }
    return 65;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.layouts count] > indexPath.row) {
        Layout *layout = (self.layouts)[indexPath.row];
        [self performSegueWithIdentifier:@"DisplayLayout" sender:layout];
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"DisplayLayout"]) {
        
        // Get destination view
        BrowserViewController *browser = [segue destinationViewController];

        
        // Pass the information to your destination view
        [browser setLayout:(Layout *)sender];
    }
}

@end
