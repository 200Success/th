//
//  FurnitureInventoryViewController.m
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "InventoryViewController.h"
#import "InventoryViewCell.h"
#import "UIImageView+TH.h"

//import data manager for Inventory
#import "InventoryManager.h"

//import detail view
#import "LayoutViewController.h"

@interface InventoryViewController ()
- (void) registerHandlerForLongPressGesture;
@end

@implementation InventoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // hide navigation control bavck button
    [self.navigationItem setHidesBackButton:YES animated:YES];
    // remove table view header offset
    self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
    // set gesture handler
    [self registerHandlerForLongPressGesture];
    
    // register table view celll identifier
    [self.tableView registerClass:[InventoryViewCell class]
           forCellReuseIdentifier:@"InventoryCell"
     ];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _furnitures = [InventoryManager sharedManager].inventory;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setTitle:@"New Layouts"];
    
    // trapping interactive pop gesture of navigation controller
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    // perform segue for show detail view
    [self performSegueWithIdentifier:@"NewLayout" sender:nil];
    _splitView =self.splitViewController.view;
    
    // set navigation pop for detail view action
    [[NSNotificationCenter defaultCenter] addObserverForName:@"CloseLayoutEditor"
                                                      object:nil
                                                       queue: [NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      [self.navigationController popToRootViewControllerAnimated:YES];
                                                  }
     ];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // clean up observer
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"CloseLayoutEditor"
                                                  object:nil];
    // clean up handler for navigation controll gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

#pragma mark - gesture recognizer delegate method

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer  {
    // Allow long press guesture to always override other guesture
    if ( [gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    //Cancel navigation controll gestue
    if ( [gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
            return NO;
    }
    return YES;
}

#pragma mark - User touch gesture

- (void) registerHandlerForLongPressGesture {
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(longPress:)];
    longPress.minimumPressDuration = 0.2; //0.2 seconds
    [self.tableView addGestureRecognizer:longPress];
    [longPress setDelegate:self];
}

- (IBAction)longPress:(UIGestureRecognizer *)sender
{
    // condition for selecting list item by long press
    // starting drag gesture
    
    LayoutViewController *detailVC;
    if (self.splitViewController.viewControllers.count > 1) {
        detailVC = (LayoutViewController *) self.splitViewController.viewControllers[1];
    }
    if(detailVC!=nil) {
        if (sender.state == UIGestureRecognizerStateBegan &&
            ![detailVC isLayoutItemSelected]) {
            // select row from  table
            CGPoint pointInTable =[sender locationInView:self.tableView];
            NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pointInTable];
            // if user press outside table rows
            // end drag gesture
            if (!indexPath) {
                _inDrag = NO;
                return;
            }
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
            if([cell isKindOfClass:[InventoryViewCell class]]) {
                [(InventoryViewCell*)cell highlightSelection];
                 _inDrag = YES;
                _selectedIndexPath = indexPath;
                UIImage *furnitureImg = [UIImage imageNamed:[[_furnitures objectAtIndex:indexPath.row] img]];
                CGRect imgRect = [cell convertRect:[(InventoryViewCell*)cell furnitureImg].frame toView:_splitView];
                _draggedView = [[UIImageView alloc] initWithFrame:imgRect];
                [_draggedView setImage:furnitureImg];
                [_splitView addSubview:_draggedView];
            }
        } // condition for drag
        else if (sender.state == UIGestureRecognizerStateChanged && _inDrag) {
            // we dragged it, so let's update the coordinates of the dragged view
            UIView *splitView = self.splitViewController.view;
            
            UIView *detailView = detailVC.layoutView;
            CGPoint point = [sender locationInView:splitView];
            _draggedView.center = point;
            
            // now add the item to the view
            [splitView addSubview:_draggedView];
            CGRect imageRect=[_splitView convertRect:_draggedView.frame toView:detailView];
            
            if (CGRectContainsRect(detailView.bounds, imageRect)) {
                if(![detailVC checkForOverlappingRect:imageRect]) {
                    [_draggedView highlightImageRectWithColor:[UIColor greenColor]];
                } else {
                    [_draggedView highlightImageRectWithColor:[UIColor redColor]];
                }
            } else {
                [_draggedView dehighlightImageRect];
            }
        } // condition on drag end
        else if (sender.state == UIGestureRecognizerStateEnded && _inDrag) {
            
            LayoutViewController *detailVC;
            if (self.splitViewController.viewControllers.count > 1) {
                detailVC = (LayoutViewController *) self.splitViewController.viewControllers[1];
            }
            UIView *detailView = detailVC.layoutView;
            
            CGRect imageRect=[_splitView convertRect:_draggedView.frame toView:detailView];
            
            if (CGRectContainsRect(detailView.bounds, imageRect)
                && ![detailVC checkForOverlappingRect:imageRect]) {
                [detailVC addFurnitureToLayoutl:(self.furnitures)[_selectedIndexPath.row] withFrame:imageRect];
                [_draggedView removeFromSuperview];
            }else {
                __block CGPoint cellCenter = [self.tableView convertPoint:[
                                                                         self.tableView cellForRowAtIndexPath:_selectedIndexPath].center
                                                                 toView:_splitView ];
                [UIView animateWithDuration: 0.4
                                    delay: 0.1
                                    options: UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                    _draggedView.alpha = 0.0;
                                    _draggedView.center = cellCenter;
                                }
                                 completion:^(BOOL finished) {
                                    if (finished) {
                                        [_draggedView removeFromSuperview];
                                        _draggedView=nil;
                                    }
                                }];
            }
            
            [(InventoryViewCell *)[self.tableView cellForRowAtIndexPath:_selectedIndexPath] dehighlightSelection];
            _selectedIndexPath = nil;
            _inDrag = NO;
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // we will display only one section
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number furniturs in nventory.
    return [self.furnitures count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InventoryViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"InventoryCell" forIndexPath:indexPath];
    Furniture *furniture = (self.furnitures)[indexPath.row];
    [cell setImage: [UIImage imageNamed:furniture.img]];
    [cell setTitle:furniture.name];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([_furnitures count]>0 ) {
        Furniture *furniture = (self.furnitures)[indexPath.row];
        CGSize imgsize = [[UIImage imageNamed:furniture.img] size];
        return imgsize.height+32.0;
    }
    return 0;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
