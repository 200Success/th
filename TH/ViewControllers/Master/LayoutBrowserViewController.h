//
//  LayoutBrowserViewController.h
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LayoutBrowserViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *layouts;

@end
