//
//  FurnitureInventoryViewController.h
//  TH
//
//  Created by Samyak Bhuta on 13/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InventoryViewController : UITableViewController <UIGestureRecognizerDelegate> {
    BOOL _inDrag;
    UIView *_splitView;
    UIImageView *_draggedView;
    NSIndexPath *_selectedIndexPath;
}

@property (strong,nonatomic) NSMutableArray *furnitures;
@end
