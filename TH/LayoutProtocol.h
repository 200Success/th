//
//  LayoutProtocol.h
//  TH
//
//  Created by Samyak Bhuta on 24/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//


#import <Foundation/Foundation.h>
@class Furniture;
@protocol LayoutProtocol <NSObject>
@required
- (void) addFurnitureToLayoutl:(Furniture *)furniture withFrame:(CGRect)frame;
- (BOOL) isLayoutItemSelected;

@end
