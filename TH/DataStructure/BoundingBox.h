//
//  BoundingBox.h
//  TH
//
//  Created by Samyak Bhuta on 15/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BoundingBox : NSObject


@property (assign, nonatomic) float x1;
@property (assign, nonatomic) float x2;
@property (assign, nonatomic) float y1;
@property (assign, nonatomic) float y2;
@property (assign, nonatomic) float width;
@property (assign, nonatomic) float height;

+ (instancetype) instanceWithRect:(CGRect)rect;
- (BOOL) intersect:(BoundingBox *)b;
- (BOOL) content:(BoundingBox *)b;
- (BOOL) contentPoint:(CGPoint)p ;
@end
