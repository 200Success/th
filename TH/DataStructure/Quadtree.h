//
//  Quadtree.h
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BoundingBox.h"
#import "BoundingBoxProtocol.h"

typedef int kLevel;
extern int const MAX_OBJ_PER_TILE;
extern int const MAX_LEVEL;
typedef enum {
    ROOT = -1,
    TOPRIGHT = 0,
    TOPLEFT = 1,
    BOTTOMLEFT = 2,
    BOTTOMRIGHT = 3
} kRegionIndex;

@interface Quadtree : NSObject {
    NSMutableArray *_branches;
    NSMutableArray *_leafs;
    kLevel _level;
}

@property (strong, nonatomic) BoundingBox *b;

+(instancetype) rootNodeWithRect:(CGRect)rect;
- (BOOL)insert:(id<BoundingBoxProtocol>)boundedObject;
- (BOOL)remove:(id<BoundingBoxProtocol>)boundedObject;
- (BOOL) doesIntersect:(CGRect)rec;
- (id<BoundingBoxProtocol>) objectAtPoint:(CGPoint) point;
@end
