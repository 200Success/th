//
//  Quadtree.m
//  TH
//
//  Created by Samyak Bhuta on 14/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "Quadtree.h"

int const MAX_OBJ_PER_TILE=4;
int const MAX_LEVEL=2;

@interface  Quadtree()
- (instancetype) initWithWithRect:(CGRect)rect andLevel:(int)level;
- (void) split;
- (void) merge;
- (int) childBranchCount;
- (int) childLeafCount;
- (BOOL) add:(id<BoundingBoxProtocol>)boundedObject;
- (kRegionIndex) findRegionForPlane: (BoundingBox *)p;

+(instancetype) newNodeWithRect:(CGRect)rect andLevel:(kLevel) level;
@end

@implementation Quadtree

+(instancetype) rootNodeWithRect:(CGRect)rect {
    return [[self alloc] initWithWithRect:rect andLevel:0];
}

+(instancetype) newNodeWithRect:(CGRect)rect andLevel:(kLevel) level {
    return [[self alloc] initWithWithRect:rect andLevel:level];
}

- (instancetype) initWithWithRect:(CGRect)rect andLevel:(int)level {
    self=[super init];
    if(self) {
        _level = level;
        _branches = [NSMutableArray array];
        _leafs = [NSMutableArray array];
        _b = [BoundingBox instanceWithRect:rect];
    }
    return self;
}


- (BOOL)insert:(id<BoundingBoxProtocol>)boundedObject {
    [boundedObject performSelector:@selector(prepareBoundingBox) withObject:nil];
    if([_b content:[boundedObject performSelector:@selector(getBoundingBox)]] ) {
        return [self add:boundedObject];
    }
    return NO;
}

- (BOOL)remove:(id<BoundingBoxProtocol>)boundedObject {
    @autoreleasepool {
        BOOL removedFromchild = NO;
        kRegionIndex index = [self findRegionForPlane:[boundedObject performSelector:@selector(getBoundingBox)] ];
        if(index==ROOT) {
            [_leafs removeObject:boundedObject];
            return  YES;
        } else {
            removedFromchild =  [(Quadtree*)[_branches objectAtIndex:index] remove:boundedObject];
        }
//        if(removedFromchild) {
//            [self merge];
//        }
        return removedFromchild;
    }
}

- (BOOL) add:(id<BoundingBoxProtocol>)boundedObject {
    @autoreleasepool {
        kRegionIndex index = [self findRegionForPlane:[boundedObject performSelector:@selector(getBoundingBox)] ];
        if(index==ROOT) {
            [_leafs addObject:boundedObject];
            if([_branches count] == 0
               && [_leafs count] > MAX_OBJ_PER_TILE
               && _level < MAX_LEVEL) {
                [self split];
            }
            return YES;
        } else {
            return [(Quadtree*)[_branches objectAtIndex:index] add:boundedObject];
        }
    }
    return  NO;
}


- (void) split {
    float mwidth = _b.width/2.0;
    float mhight = _b.height/2.0;
    float mx = _b.x1/2.0 + _b.x2/2.0;
    float my = _b.y1/2.0 + _b.y2/2.0;
    
    // Insert Quard tiles in the order of rigion index
    [_branches addObject:
                        [Quadtree newNodeWithRect:CGRectMake(_b.x1, _b.y1, mwidth, mhight) andLevel:_level +1]];
    [_branches addObject:
                        [Quadtree newNodeWithRect:CGRectMake(mx, _b.y1, mwidth, mhight) andLevel:_level +1]];
    [_branches addObject:
                        [Quadtree newNodeWithRect:CGRectMake(mx, my, mwidth, mhight) andLevel:_level +1]];
    [_branches addObject:
                        [Quadtree newNodeWithRect:CGRectMake(_b.x1, my, mwidth, mhight) andLevel:_level +1]];
    
        NSArray *leafBucket = [NSArray arrayWithArray:_leafs];
        _leafs=[NSMutableArray array];
        for (id<BoundingBoxProtocol> boundedObject in leafBucket) {
                [self add:boundedObject];
        }
}

//- (void) merge {
//    int grandChildNodes = 0;
//    int grandChildLeafs = 0;
//    for (int i=0; i<[_branches count]; i++) {
//        grandChildNodes = [(Quadtree*)[_branches objectAtIndex:i] childBranchCount];
//        grandChildLeafs = [(Quadtree*)[_branches objectAtIndex:i] childBranchCount];
//    }
//    if(grandChildNodes ==0 &&
//       (grandChildLeafs==0 ||
//        (grandChildNodes+[self childBranchCount] <= MAX_OBJ_PER_TILE)
//        )) {
//           for (int i=0; i<[_branches count]; i++) {
//               grandChildNodes = [(Quadtree*)[_branches objectAtIndex:i] childBranchCount];
//               grandChildLeafs = [(Quadtree*)[_branches objectAtIndex:i] childBranchCount];
//           }
//    }
//}

- (int) childBranchCount {
    return [_branches count];
}

- (int) childLeafCount {
    return [_leafs count];
}


- (kRegionIndex) findRegionForPlane: (BoundingBox *)p {
    for (int i=0; i<[_branches count]; i++) {
        if([[(Quadtree*)[_branches objectAtIndex:i] b] content: p]){
            return i;
        }
    }
    return ROOT;
}

- (BOOL) doesIntersect:(CGRect)rect  {
    @autoreleasepool {
        BoundingBox *p = [BoundingBox instanceWithRect:rect];
        for(id<BoundingBoxProtocol> boundedObject in _leafs) {
            if([[boundedObject performSelector:@selector(getBoundingBox)] intersect:p]) {
                return true;
            }
        }
        
        for(Quadtree *q in _branches) {
            if([q.b content:p]) {
                return [q doesIntersect:rect];
            }
        }
    }
    return NO;
}

- (id<BoundingBoxProtocol>) objectAtPoint:(CGPoint) p {
    @autoreleasepool {
        for(id<BoundingBoxProtocol> boundedObject in _leafs) {
            if([[boundedObject performSelector:@selector(getBoundingBox)] contentPoint:p]) {
                return boundedObject;
            }
        }
        
        for(Quadtree *q in _branches) {
            if([q.b contentPoint:p]) {
                return [q objectAtPoint:p];
            }
        }
    }
    return nil;
}
@end
