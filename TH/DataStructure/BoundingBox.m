//
//  BoundingBox.m
//  TH
//
//  Created by Samyak Bhuta on 15/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "BoundingBox.h"

@implementation BoundingBox

+ (instancetype) instanceWithRect:(CGRect)rect {
    BoundingBox *b =nil;
    @autoreleasepool {
        b = [[BoundingBox alloc] init];
        b.x1 = rect.origin.x;
        b.y1 = rect.origin.y;
        b.x2 = rect.origin.x+rect.size.width;
        b.y2 = rect.origin.y+rect.size.height;
        b.width = rect.size.width;
        b.height = rect.size.height;
    }
    return b;
}


-(BOOL) intersect:(BoundingBox *)b {
    return self.x1 < b.x2 &&
            self.y1 < b.y2 &&
            self.x2 > b.x1 &&
            self.y2 > b.y1;
}

- (BOOL) content:(BoundingBox *)b {
    return self.x1 <= b.x1 &&
            self.y1 <= b.y1 &&
            self.x2 >= b.x2 &&
            self.y2 >= b.y2;
}

- (BOOL) contentPoint:(CGPoint)p {
    return self.x1 <= p.x &&
    self.y1 <= p.y &&
    self.x2 >= p.x &&
    self.y2 >= p.y;
}

@end
