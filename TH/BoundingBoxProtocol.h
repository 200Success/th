//
//  BoundingBoxProtocol.h
//  TH
//
//  Created by Samyak Bhuta on 21/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BoundingBox;
@protocol BoundingBoxProtocol <NSObject>

@required
- (BoundingBox *)getBoundingBox;
- (void)prepareBoundingBox;

@end