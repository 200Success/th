//
//  UIImageView+TH.m
//  TH
//
//  Created by Samyak Bhuta on 19/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "UIImageView+TH.h"

@implementation UIImageView(TH)

- (void)highlightImageRectWithColor:(UIColor*)color {
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(-1, -1,
                                    (self.frame.size.width)+1,
                                    (self.frame.size.height+1));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setBorderWidth:1.0];
    [borderLayer setBorderColor:[color CGColor]];
    [self.layer addSublayer:borderLayer];
}

- (void)dehighlightImageRect {
    self.layer.sublayers=nil;
}

@end
