//
//  CATextLayer+TH.m
//  TH
//
//  Created by Samyak Bhuta on 21/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import "CATextLayer+TH.h"
#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface CATextLayer (_TH)

- (NSAttributedString *)attributedString;

@end

@implementation CATextLayer (_TH)

- (NSAttributedString *)attributedString {
    if ([self.string isKindOfClass:[NSAttributedString class]]) {
        return self.string;
    }
    NSString *string = self.string;
    CTFontRef layerFont = self.font;
    CFStringRef fname = CTFontCopyPostScriptName(layerFont);
    CTFontRef font = CTFontCreateWithName(fname, self.fontSize, NULL);
    CTTextAlignment alignment;
    
    if ([self.alignmentMode isEqualToString:kCAAlignmentLeft]) {
        alignment = kCTLeftTextAlignment;
        
    } else if ([self.alignmentMode isEqualToString:kCAAlignmentRight]) {
        alignment = kCTRightTextAlignment;
        
    } else if ([self.alignmentMode isEqualToString:kCAAlignmentCenter]) {
        alignment = kCTCenterTextAlignment;
        
    } else if ([self.alignmentMode isEqualToString:kCAAlignmentJustified]) {
        alignment = kCTJustifiedTextAlignment;
        
    } else if ([self.alignmentMode isEqualToString:kCAAlignmentNatural]) {
        alignment = kCTNaturalTextAlignment;
    }
    
    // Process the information to get an attributed string
    CFMutableAttributedStringRef attrString = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0);
    
    if (string != nil) {
        CFAttributedStringReplaceString (attrString, CFRangeMake(0, 0), (CFStringRef)string);
    }
    
    CFAttributedStringSetAttribute(attrString, CFRangeMake(0, CFAttributedStringGetLength(attrString)), kCTFontAttributeName, font);
    
    CTParagraphStyleSetting settings[] = {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment};
    CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(settings, sizeof(settings) / sizeof(settings[0]));
    CFAttributedStringSetAttribute(attrString, CFRangeMake(0, CFAttributedStringGetLength(attrString)), kCTParagraphStyleAttributeName, paragraphStyle);
    CFRelease(paragraphStyle);
    
    NSMutableAttributedString *ret = (__bridge NSMutableAttributedString *)attrString;
    
    return ret;
}

@end

@implementation CATextLayer (TH)

- (void) adjustToFit {
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString( (CFMutableAttributedStringRef) [self attributedString]);
    CGSize suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0), NULL, CGSizeMake(self.bounds.size.width, CGFLOAT_MAX), NULL);
   self.bounds = CGRectMake(self.bounds.origin.x,
                             self.bounds.origin.y,
                             self.bounds.size.width,
                             suggestedSize.height);
    self.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.3f] CGColor];
    if(suggestedSize.height > self.bounds.size.height) {
        CGFloat scale = self.bounds.size.height/suggestedSize.height;
        self.fontSize = ceil(self.fontSize*scale);
    } else {
        self.bounds = CGRectMake(self.bounds.origin.x,
                                 self.bounds.origin.y,
                                 self.bounds.size.width,
                                 suggestedSize.height);
    }
}


@end
