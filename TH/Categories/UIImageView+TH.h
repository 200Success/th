//
//  UIImageView+TH.h
//  TH
//
//  Created by Samyak Bhuta on 19/06/15.
//  Copyright (c) 2015 Sourav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView(TH)

- (void)highlightImageRectWithColor:(UIColor*)color;
- (void)dehighlightImageRect;

@end
